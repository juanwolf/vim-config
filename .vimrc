execute pathogen#infect()

set t_Co=256
syntax on
filetype plugin indent on
colorscheme solarized

" Editor configuration "
set tabstop=4

set background=dark

" Add number margin "
set number

" Make backspace work like most other apps
set backspace=2
set tabstop=4
set shiftwidth=4
set expandtab

" Removing trailing whitespaces every write operations
autocmd BufWritePre * %s/\s\+$//e

" Vim Airline configuration "
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline_theme = 'luna'

" Setting up flake8 "
autocmd BufWritePost *.py call Flake8()
let g:flake8_show_in_gutter=1  " show

" Set F6 to switch brackground color "
map <F6> :let &background = ( &background == "dark"? "light" : "dark" )<CR>
map <F3> :NERDTreeToggle<CR>

" Changing split navigations shortcuts "
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
